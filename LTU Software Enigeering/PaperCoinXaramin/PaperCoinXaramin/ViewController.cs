﻿using System;                           //Used temp for Random in Register
using System.ComponentModel;
using System.Text;
using System.Collections.Generic;       //Used temporarily for List
using MongoDB.Driver;                   //possible temp
using MongoDB.Bson;
using MongoDB;

namespace PaperCoinXaramin
{
    public partial class ViewController : INotifyPropertyChanged
    {
        //=================================================
        //============ APPLICATION PROPERTIES =============
        //=================================================
    
        User _UserInstance;
        public User UserInstance
        {
            get
            {
                return _UserInstance;
            }
            set
            {
                _UserInstance = value;
                UserName = value.user;

                if (UserInstance.email != " ")
                    UserInstance.isLoggedIn = "true";

                if (UserInstance.isLoggedIn == "true")
                {
                    IsLoggedIn = true;
                }
                else if (UserInstance.isLoggedIn == "false")
                {
                    IsLoggedIn = false;
                }
            }
        }

        Coin _CoinInstance;
        public Coin CoinInstance
        {
            get
            {
                return _CoinInstance;
            }
            set
            {
                _CoinInstance = value;

                CoinTicker = CoinInstance.ticker;
                CoinAskPrice = CoinInstance.askPrice;
                CoinBidPrice = CoinInstance.bidPrice;
                CoinMarketPrice = CoinInstance.marketPrice;
                CoinVolume = CoinInstance.volume;
            }
        }

        Holdings _HoldingInstance;
        public Holdings HoldingsInstance
        {
            get
            {
                return _HoldingInstance;
            }
            set
            {

                UsdAmount = value.Amount[0];
                BtcAmount = value.Amount[1];
                EthAmount = value.Amount[2];
                BchAmount = value.Amount[3];
                XplAmount = value.Amount[4];
                LtcAmount = value.Amount[5];

                OnPropertyChanged("UsdAmount");
                OnPropertyChanged("BtcAmount");
                OnPropertyChanged("EthAmount");
                OnPropertyChanged("BchAmount");
                OnPropertyChanged("XplAmount");
                OnPropertyChanged("LtcAmount");
            }
        }

        bool _IsLoggedIn;
        public bool IsLoggedIn
        {
            get
            {
                return _IsLoggedIn;
            }
            set
            {
                if (value == true)
                {
                    _IsLoggedIn = true;
                    WelcomeViewVisible = false;
                    ProfileViewVisible = true;
                    OnPropertyChanged("IsLoggedIn");
                }
                else if (value == false)
                {
                    _IsLoggedIn = false;
                    WelcomeViewVisible = true;
                    ProfileViewVisible = false;
                    OnPropertyChanged("IsLoggedIn");
                }
            }
        }

        bool _WelcomeViewVisible;
        public bool WelcomeViewVisible
        {
            get
            {
                return _WelcomeViewVisible;
            }
            set
            {
                _WelcomeViewVisible = value;
                OnPropertyChanged("WelcomeViewVisible");
            }
        }

        bool _ProfileViewVisible;
        public bool ProfileViewVisible
        {
            get
            {
                return _ProfileViewVisible;
            }
            set
            {
                _ProfileViewVisible = value;
                OnPropertyChanged("ProfileViewVisible");
            }
        }

        string _UserName;
        public string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {
                _UserName = value;
                OnPropertyChanged("UserName");
            }
        }

        string _CoinTicker;
        public string CoinTicker
        {
            get
            {
                return _CoinTicker;
            }
            set
            {
                bool coinFound = false;
                value = value.ToUpper();

                if (value != CoinInstance.ticker)
                {
                    foreach (Coin e in TestCoinData)
                    {
                        if (e.ticker == value)
                        {
                            _CoinTicker = value;
                            CoinInstance = e;
                            coinFound = true;
                        }
                    }

                    if (!coinFound)
                    {
                        CoinInstance = new Coin();
                    }
                }
            }
        }
        
        public float CoinAskPrice { get; set; }

        public float CoinBidPrice { get; set; }

        public float CoinMarketPrice { get; set; }

        public float CoinVolume { get; set; }

        public string CoinConfirmNum { get; set; }

        string _ConfirmMode;
        public string ConfirmMode
        {
            get
            {
                return _ConfirmMode;
            }
            set
            {
                _ConfirmMode = value;
            }
        }

        float _OrderAmount;
        public float OrderAmount
        {
            get
            {
                return _OrderAmount;
            }
            set
            {
                _OrderAmount = value;
            }
        }

        float _OrderTotal;
        public float OrderTotal
        {
            get
            {
                return _OrderTotal;
            }
            set
            {
                _OrderTotal = value;
            }
        }

        //Terrible programming practice incoming... Fix this!!!

        public float _UsdAmount;
        public float UsdAmount
        {
            get
            {
                return _UsdAmount;
            }
            set
            {
                _UsdAmount = value;
            }
        }

        public float _BtcAmount;
        public float BtcAmount
        {
            get
            {
                return _BtcAmount;
            }
            set
            {
                _BtcAmount = value;
            }
        }

        public float _EthAmount;
        public float EthAmount
        {
            get
            {
                return _EthAmount;
            }
            set
            {
                _EthAmount = value;
            }
        }

        public float _BchAmount;
        public float BchAmount
        {
            get
            {
                return _BchAmount;
            }
            set
            {
                _BchAmount = value;
            }
        }

        public float _XplAmount;
        public float XplAmount
        {
            get
            {
                return _XplAmount;
            }
            set
            {
                _XplAmount = value;
            }
        }

        public float _LtcAmount;
        public float LtcAmount
        {
            get
            {
                return _LtcAmount;
            }
            set
            {
                _LtcAmount = value;
            }
        }

        //=================================================
        //============== APPLICATION METHODS ==============
        //=================================================

        public bool ClientReq_Login(string email, string password)
        {
            email = email.Trim();

            foreach (User e in TestUserData)
            {
                if (e.email == email && e.pass == password)
                {
                    UserInstance = e;
                    return true;
                }
            }

            return false;
        }

        public bool ClientReq_Register(string username, string email, string password)
        {
            Random r = new Random();
            int userId = r.Next(1000, 10000);

            username = username.Trim();
            email = email.Trim();

            foreach (User e in TestUserData)
            {
                if (e.email == email)
                {
                    return false;
                }
            }

            User newUser = new User(username, userId, password, email, "false", "true");
            TestUserData.Add(newUser);
            UserInstance = newUser;

            var coinList = new List<string>();
            coinList.Add("USD");
            coinList.Add("BTC");
            coinList.Add("ETH");
            coinList.Add("BCH");
            coinList.Add("XPL");
            coinList.Add("LTC");

            float[] input =
            {
                50000,
                0,
                0,
                0,
                0,
                0
            };

            Holdings newHoldings = new Holdings(email, coinList, new List<float>(input));
            HoldingsInstance = newHoldings;

            return true;
        }

        
        public bool ClientReq_PlaceOrder()
        {
            int coinPos = 0;

            // Hard coded.... This should be changed
            switch(CoinTicker)
            {
                case "BTC":
                    coinPos = 1;
                    break;
                case "ETH":
                    coinPos = 2;
                    break;
                case "BCH":
                    coinPos = 3;
                    break;
                case "XPL":
                    coinPos = 4;
                    break;
                case "LTC":
                    coinPos = 5;
                    break;
                default:
                    break;
            }

            if (ConfirmMode == "Buy")
            {
                HoldingsInstance.Amount[0] -= OrderTotal;
                HoldingsInstance.Amount[coinPos] += OrderAmount;
            }
            else if (ConfirmMode == "Sell")
            {
                HoldingsInstance.Amount[0] += OrderTotal;
                HoldingsInstance.Amount[coinPos] -= OrderAmount;
            }
            else
            {
                // This should never be reached, but is here as a catch
                return false;
            }

            return true;
        }

        public bool InternalReq_CalculateTotal()
        {
            OrderTotal = OrderAmount * CoinMarketPrice;

            return true;
        }


        //=================================================
        //============= APPLICATION TEST DATA =============
        //=================================================

        List<Coin> TestCoinData = new List<Coin>();

        List<User> TestUserData = new List<User>();

        List<Holdings> TestHoldingsData = new List<Holdings>();

        //=================================================
        //================== CONSTRUCTOR ==================
        //=================================================

        public ViewController()
        { 
            //Filling test data...

            TestCoinData.Add(new Coin(1111, 1111, 1111, 1111, "BTC", "XXX"));
            TestCoinData.Add(new Coin(2222, 2222, 2222, 2222, "ETH", "XXX"));
            TestCoinData.Add(new Coin(3333, 3333, 3333, 3333, "BCH", "XXX"));
            TestCoinData.Add(new Coin(4444, 4444, 4444, 4444, "XPL", "XXX"));
            TestCoinData.Add(new Coin(5555, 5555, 5555, 5555, "LTC", "XXX"));

            TestUserData.Add(new User("Gary Sweet", 1111, "GaSw", "GarySweet@example.com", "false", "false"));
            TestUserData.Add(new User("John Doe", 2222, "JoDo", "JohnDoe@example.com", "false", "false"));
            TestUserData.Add(new User("Jane Doe", 3333, "JaDo", "JohnDoe@example.com", "false", "false"));
            TestUserData.Add(new User("Working Otter", 4444, "WoOt", "WorkingOtter@example.com", "false", "false"));
            TestUserData.Add(new User("El Bathy", 5555, "ElBa", "ElBathy@example.com", "false", "false"));

            var coinList = new List<string>();
            coinList.Add("USD");
            coinList.Add("BTC");
            coinList.Add("ETH");
            coinList.Add("BCH");
            coinList.Add("XPL");
            coinList.Add("LTC");

            float[] input =
            {
                1000,
                12,
                4523,
                1231,
                23,
                435
            };

            TestHoldingsData.Add(new Holdings("GarySweet@example.com", coinList, new List<float>(input)));
            TestHoldingsData.Add(new Holdings("JohnDoe@example.com", coinList, new List<float>(input)));
            TestHoldingsData.Add(new Holdings("JohnDoe@example.com", coinList, new List<float>(input)));
            TestHoldingsData.Add(new Holdings("WorkingOtter@example.com", coinList, new List<float>(input)));
            TestHoldingsData.Add(new Holdings("ElBathy@example.com", coinList, new List<float>(input)));

            //End test data.

            CoinInstance = new Coin();
            //UserInstance = new User();
            UserInstance = TestUserData[0];
            HoldingsInstance = TestHoldingsData[0];

            // <insert a long paragraph about how much I really hate computers here>
            // Because considering this damn function won't work, I really do
            // var client = ConnectDB.ConnectReplicaSet();

            if (UserInstance.isLoggedIn == "true")
            {
                IsLoggedIn = true;
                WelcomeViewVisible = false;
                ProfileViewVisible = true;
            }
            else
            {
                IsLoggedIn = false;
                WelcomeViewVisible = true;
                ProfileViewVisible = false;
            }

            UserName = UserInstance.user;
        }

        //=================================================
        //============ EVENT HANDLER OVERRIDE =============
        //=================================================

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var changed = PropertyChanged;
            if (changed != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
