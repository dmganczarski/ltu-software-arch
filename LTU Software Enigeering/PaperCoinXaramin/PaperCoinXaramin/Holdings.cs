﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaperCoinXaramin
{
    public class Holdings
    {
        public string Email { get; set; }
        List<string> _Coins;
        public List<string> Coins
        {
            get
            {
                List<string> retList = new List<string>();
                foreach (string e in _Coins)
                {
                    retList.Add(e);
                }

                return retList;
            }
            set
            {
                _Coins = value;
            }
        }
        List<float> _Amount;
        public List<float> Amount
        {
            get
            {
                List<float> retList = new List<float>();
                foreach (float e in _Amount)
                {
                    retList.Add(e);
                }

                return retList;
            }
            set
            {
                _Amount = value;
            }
        }

        public Holdings()
        {
            this.Email = "";
            this.Coins = new List<string>();
            this.Amount = new List<float>();
        }

        public Holdings(string email, List<string> coins, List<float> amount)
        {
            this.Email = email;
            this.Coins = coins;
            this.Amount = amount;
        }

        ~Holdings()
        {
            this.Email = null;
            this.Coins = null;
            this.Amount = null;
        }
    }
}
