﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PaperCoinXaramin
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
		public LoginPage ()
		{
			InitializeComponent ();
		}

        void ClickedSubmitButton(object sender, EventArgs e)
        {
            if (App.AppView.ClientReq_Login(EmailEntry.Text, PasswordEntry.Text))
            {
                DisplayAlert("", "Login Successful!", "OK");
            }
        }
	}
}