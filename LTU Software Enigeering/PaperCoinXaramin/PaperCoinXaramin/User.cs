﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperCoinXaramin
{
    public class User
    {
        public string user { get; set; }
        public int userID { get; set; }
        public string email { get; set; }
        public string pass { get; set; }
        //public string[] holdings { get; set; }
        //public float[] amount { get; set; }
        public string twofact { get; set; }
        public string isLoggedIn { get; set; }

        public User()
        {
            this.user = " ";
            this.userID = 0000;
            this.email = " ";
            this.pass = " ";
            this.twofact = " ";
            this.isLoggedIn = " ";
        }

        public User(string userName, int ID, string password, string emailAddress, string twoFactor, string loggedin)
        {
            this.user = userName;
            this.userID = ID;
            this.email = emailAddress;
            this.pass = password;
            this.twofact = twoFactor;
            this.isLoggedIn = loggedin;
        }
        ~User()
        {
            Console.WriteLine("User " + this.user + "#" + this.userID + " deleted");
            this.user = null;
            this.userID = 0;
            this.email = null;
            this.pass = null;
            this.twofact = null;
            this.isLoggedIn = null;

        }
    }
}
