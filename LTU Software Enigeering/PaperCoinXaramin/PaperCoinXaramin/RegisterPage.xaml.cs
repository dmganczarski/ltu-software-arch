﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PaperCoinXaramin
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegisterPage : ContentPage
	{
		public RegisterPage ()
		{
			InitializeComponent ();
		}

        void ClickedRegisterButton(object sender, EventArgs e)
        {
            var username = UsernameEntry.Text;
            var email = EmailEntry.Text;
            var confirmEmail = ConfirmEmailEntry.Text;
            var password = PasswordEntry.Text;
            var confirmPassword = ConfirmPasswordEntry.Text;

            if (email == confirmEmail && password == confirmPassword)
            {
                if (App.AppView.ClientReq_Register(username, email, password))
                {
                    DisplayAlert("", "Account successfully created!", "OK");
                }
            }
        }
    }
}