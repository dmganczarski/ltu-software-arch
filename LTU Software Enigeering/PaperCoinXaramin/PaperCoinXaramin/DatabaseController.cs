using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB;

namespace PaperCoinXaramin
{
    public class DatabaseController
    {
        string shardName = "mongodb://kay:GarySweet@papercoindb-shard-00-00-hrs16.mongodb.net:27017," +
            "papercoindb-shard-00-01-hrs16.mongodb.net:27017," +
            "papercoindb-shard-00-02-hrs16.mongodb.net:27017/admin?ssl=true&replicaSet=Mycluster0-shard-0&authSource=admin";
        string shardOne = "papercoindb-shard-00-00-hrs16.mongodb.net:27017";
        string shardTwo = "papercoindb-shard-00-01-hrs16.mongodb.net:27017";
        string shardThree = "papercoindb-shard-00-02-hrs16.mongodb.net:27017";

        static void AnotherMain(string[] args)
        {
            var client = new MongoClient("mongodb://key:GarySweet@papercoindb-shard-00-00-hrs16.mongodb.net:27017," +
            "papercoindb-shard-00-01-hrs16.mongodb.net:27017," +
            "papercoindb-shard-00-02-hrs16.mongodb.net:27017/admin?ssl=true&replicaSet=Mycluster0-shard-0&authSource=admin");

            var database = client.GetDatabase("test");
            var userCollection = database.GetCollection<User>("users");
            var coinCollection = database.GetCollection<Coin>("coins");
            var transCollection = database.GetCollection<Transaction>("transactions");

            string text = System.IO.File.ReadAllText(@"C:\Users\Jason\Desktop\generated2.json");
            //var doc = Bson
            //text.ToBson();
            var doc = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonArray>(text);

            User jason = new User();
            jason.user = "Jay";
            jason.userID = 9461;
            jason.email = "pleasework@gmail.com";
            jason.pass = "Please";
            //jason.holdings[0] = "ETH";
            //jason.amount[0] = 12.345f;
            jason.twofact = "true";
            jason.isLoggedIn = "true";

            Transaction trans = new Transaction("Jay", 9461, "20180414_173414", "BTC", 1.23f, "ETH", 4.624f, "9461 20180414_173414");
            Coin bitcoin = new Coin(9573.49f, 9573.90f, 9573.65f, 2345.954f, "BTC", "20180414_173414");

            userCollection.InsertOneAsync(jason);

            //BsonDocument arrToDoc = text.ToBsonDocument();

            // var bsonDoc = BsonDocument.Parse(text);

            Console.WriteLine(jason);

            Console.WriteLine(doc + "                         " + database);


            Console.ReadLine();
        }

    }


    public class RootUser
    {
        int numOfusers = 0;
        public User[] Property1 { get; set; }

        public RootUser(User u)
        {
            Property1[numOfusers] = u;
            numOfusers++;
        }

    }
}
