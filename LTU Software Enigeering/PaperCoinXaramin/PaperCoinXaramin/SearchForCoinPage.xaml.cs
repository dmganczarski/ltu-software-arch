﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PaperCoinXaramin
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SearchForCoinPage : ContentPage
	{
		public SearchForCoinPage ()
		{
			InitializeComponent ();
		}

        async Task ClickedSearchButton(object sender, EventArgs e)
        {
            App.AppView.CoinTicker = SymbolEntry.Text;

            await Navigation.PushAsync(new CoinPage());
        }
	}
}