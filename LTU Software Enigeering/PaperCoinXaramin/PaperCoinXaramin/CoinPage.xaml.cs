﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PaperCoinXaramin
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CoinPage : ContentPage
	{
		public CoinPage ()
		{
			InitializeComponent ();
            BindingContext = App.AppView;
        }

        async Task ClickedBuyButton(object sender, EventArgs e)
        {
            App.AppView.ConfirmMode = "Buy";
            await Navigation.PushAsync(new TransactionPage());
        }

        async Task ClickedSellButton(object sender, EventArgs e)
        {
            App.AppView.ConfirmMode = "Sell";

            await Navigation.PushAsync(new TransactionPage());
        }
    }
}