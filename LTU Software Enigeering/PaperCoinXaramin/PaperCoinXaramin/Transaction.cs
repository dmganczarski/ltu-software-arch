﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperCoinXaramin
{
    public class Transaction
    {
        public string Username { get; set; }
        public int ID { get; set; }
        public string Date { get; set; }
        public string SendCoin { get; set; }
        public float SendAmount { get; set; }
        public string RecieveCoin { get; set; }
        public float RecieveAmount { get; set; }
        public string ConfirmationNumber { get; set; }

        public Transaction()
        {
            this.Username = " ";
            this.ID = 0;
            this.Date = "yyyymmdd_hhmmss";
            this.SendCoin = " ";
            this.SendAmount = 0.0f;
            this.RecieveCoin = " ";
            this.RecieveAmount = 0.0f;
            this.ConfirmationNumber = "No Confirmation";
        }
        public Transaction(string user, int id, string date, string sCoin, float sAmt, string rCoin, float rAmt, string confim)
        {
            this.Username = user;
            this.ID = id;
            this.Date = date;
            this.SendCoin = sCoin;
            this.SendAmount = sAmt;
            this.RecieveCoin = rCoin;
            this.RecieveAmount = rAmt;
            this.ConfirmationNumber = ConfirmationNumber + date;
        }
        ~Transaction()
        {
            this.Username = null;
            this.ID = 0;
            this.Date = null;
            this.SendCoin = null;
            this.SendAmount = 0;
            this.RecieveCoin = null;
            this.RecieveAmount = 0;
            this.ConfirmationNumber = null;
        }

    }
}
