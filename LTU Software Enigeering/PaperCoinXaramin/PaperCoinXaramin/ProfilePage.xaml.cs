﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PaperCoinXaramin
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ProfilePage : ContentPage
	{
		public ProfilePage()
		{
			InitializeComponent();
            BindingContext = App.AppView;
		}

        async Task ClickedRegisterButton(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RegisterPage());
        }

        async Task ClickedSignInButton(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new LoginPage());
        }

        async Task ClickedViewHoldingsButton(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new HoldingsPage());
        }

        void ClickedLogOutButton(object sender, EventArgs e)
        {
            App.AppView.IsLoggedIn = false;
        }
    }
}