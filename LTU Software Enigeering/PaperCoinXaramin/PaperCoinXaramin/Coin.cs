﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperCoinXaramin
{
    public class Coin
    {
        public float bidPrice { get; set; }
        public float askPrice { get; set; }
        public float marketPrice { get; set; }
        public float volume { get; set; }
        public string ticker { get; set; }
        public string lastUpdateTime { get; set; }
        public string confirmationNumber { get; set; }

        public Coin()
        {
            this.bidPrice = 0.0f;
            this.askPrice = 0.0f;
            this.marketPrice = 0.0f;
            this.volume = 0.0f;
            this.ticker = " ";
            this.lastUpdateTime = "yyyymmdd_hhmmss";
        }
        public Coin(float bid, float ask, float market, float vol, string tick, string LUT)
        {
            this.bidPrice = bid;
            this.askPrice = ask;
            this.marketPrice = market;
            this.volume = vol;
            this.ticker = tick;
            this.lastUpdateTime = LUT;
        }
        ~Coin()
        {
            this.bidPrice = 0;
            this.askPrice = 0;
            this.marketPrice = 0;
            this.volume = 0;
            this.ticker = null;
            this.lastUpdateTime = null;
        }
    }
}
