﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;

namespace PaperCoinXaramin
{
    public class ConnectDB 
    {
        public static MongoClient ConnectReplicaSet()
        {
            string shardName = "mongodb://kay:GarySweet@papercoindb-shard-00-00-hrs16.mongodb.net:27017," +
            "papercoindb-shard-00-01-hrs16.mongodb.net:27017," +
            "papercoindb-shard-00-02-hrs16.mongodb.net:27017/admin?ssl=true&replicaSet=Mycluster0-shard-0&authSource=admin";
            string shardOne = "papercoindb-shard-00-00-hrs16.mongodb.net:27017";
            string shardTwo = "papercoindb-shard-00-01-hrs16.mongodb.net:27017";
            string shardThree = "papercoindb-shard-00-02-hrs16.mongodb.net:27017";

            var settings = new MongoClientSettings
            {
                Servers = new[]
                {
                    new MongoServerAddress("papercoindb-shard-00-00-hrs16.mongodb.net", 27017),
                    new MongoServerAddress("papercoindb-shard-00-01-hrs16.mongodb.net", 27017),
                    new MongoServerAddress("papercoindb-shard-00-02-hrs16.mongodb.net", 27017)
                },
                ConnectionMode = ConnectionMode.ReplicaSet,
                ReplicaSetName = "PaperCoinDB",
                WriteConcern = new WriteConcern(WriteConcern.WValue.Parse("3"), wTimeout: TimeSpan.Parse("10"))
            };

            //var client = new MongoClient("mongodb://kay:GarySweet@papercoindb-shard-00-00-hrs16.mongodb.net:27017," +
            //"papercoindb-shard-00-01-hrs16.mongodb.net:27017," +
            //"papercoindb-shard-00-02-hrs16.mongodb.net:27017/admin?ssl=true&replicaSet=Mycluster0-shard-0&authSource=admin");



            //var client = new MongoClient(settings);
            var newClient = new MongoClient("mongodb://admin:GarySweet@papercoindb-shard-00-01-hrs16.mongodb.net:27017,papercoindb-shard-00-00-hrs16.mongodb.net:27017,papercoindb-shard-00-02-hrs16.mongodb.net:27017/PaperCoinDB?connect=replicaSet");

            return newClient;

        }

    }
}
