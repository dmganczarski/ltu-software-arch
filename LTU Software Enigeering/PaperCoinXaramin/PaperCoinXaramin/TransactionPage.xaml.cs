﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PaperCoinXaramin
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TransactionPage : ContentPage
	{
		public TransactionPage ()
		{
			InitializeComponent ();
            BindingContext = App.AppView;
		}

        async Task ClickedConfirmOrderButton(object sender, EventArgs e)
        {
            App.AppView.OrderAmount = float.Parse(AmountEntry.Text, System.Globalization.CultureInfo.InvariantCulture);
            App.AppView.InternalReq_CalculateTotal();

            await Navigation.PushAsync(new ConfirmationPage());
        }
	}
}