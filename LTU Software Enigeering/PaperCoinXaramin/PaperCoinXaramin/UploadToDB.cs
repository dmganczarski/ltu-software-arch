﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Driver.Linq;

namespace PaperCoinXaramin
{
    class UploadToDB
    {
        public async static void UploadUserBson(MongoClient client, BsonDocument b)
        {
            //MongoClient client = ConnectDB.ConnectReplicaSet();
            IMongoDatabase database = client.GetDatabase("test");
            IMongoCollection<BsonDocument> collection = database.GetCollection<BsonDocument>("users");

            //await collection.InsertOneAsync(b);
        }
        public async static void UploadUserObject(MongoClient client, User u)
        {
            //MongoClient client = ConnectDB.ConnectReplicaSet();
            IMongoDatabase database = client.GetDatabase("test");
            IMongoCollection<User> collection = database.GetCollection<User>("users");

            await collection.InsertOneAsync(u);
        }
        public async static void UploadCoinBson(MongoClient client, BsonDocument b)
        {
            //var client = ConnectDB.ConnectReplicaSet();
            var database = client.GetDatabase("test");
            var collection = database.GetCollection<BsonDocument>("coins");

            await collection.InsertOneAsync(b);
        }
        public async static void UploadCoinObject(MongoClient client, Coin c)
        {
            var database = client.GetDatabase("test");
            var collection = database.GetCollection<Coin>("coins");

            await collection.InsertOneAsync(c);
        }
        public async static void UploadTransBson(MongoClient client, BsonDocument b)
        {
            //var client = ConnectDB.ConnectReplicaSet();
            var database = client.GetDatabase("test");
            var collection = database.GetCollection<BsonDocument>("transactions");

            await collection.InsertOneAsync(b);
        }
        public async static void UploadTransObject(MongoClient client, Transaction t)
        {
            var database = client.GetDatabase("test");
            var collection = database.GetCollection<Transaction>("transactions");

            await collection.InsertOneAsync(t);
        }
    }
}
